# Tech Junction

Tech Junction, a remote software engineering job portal, is built using React, React Query, TypeScript, JavaScript, HTML, and CSS. This application enables users to search for software engineering, web development, and other jobs. Users can view individual job details, sort jobs by relevance and recency, and bookmark jobs. Bookmarks are stored in the browser's local storage.


## [Live Application](https://techjunction.vercel.app/)

![App Screenshot 1](public/1.png)
![App Screenshot 2](public/2.png)
![App Screenshot 3](public/3.png)
![App Screenshot 4](public/4.png)

## Features

- Built with React, React Query, TypeScript, JavaScript, HTML, and CSS.
- Allows job searches in software engineering, web development, etc.
- View job details.
- Sort jobs by relevance and recency.
- Bookmark jobs.
- Bookmarks stored locally.

## Tech Stack
- React
- React Query
- React Hot Toast
- Typescript
- Javascript
- HTML
- CSS

**Hosting:**
- Vercel

## Run Locally

Clone the project

```bash
  git clone https://gitlab.com/srabon444/techjunction.git
```

Install dependencies

```bash
  npm install
```
