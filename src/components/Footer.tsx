export default function Footer() {
  return (
    <footer className="footer">
      <small>
        <p>
          © Copyright by{" "}
          <a href="https://www.linkedin.com/in/ashraful-islam-rabby/" target="_blank">
            Ashraful Islam
          </a>
          . Intended for learning.
        </p>
      </small>

      <p>
        <span className="u-bold">102573</span> total jobs available
      </p>
    </footer>
  );
}
