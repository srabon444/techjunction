import logo from "../../public/logo.png";

export default function Logo() {
    return (
        <a href="." className="logo">
            <img width={150}
                 src={logo}
                 alt="Logo"
                 className="logo__img"
            />
        </a>
    );
}
